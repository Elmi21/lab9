#include <stdio.h>
#include <string.h>
#include "message.h"

void sys_err(char *msg)
{
    puts(msg);
    exit(1);
}

int main(int argc, char * argv[])
{
    int semid;
    int shmid;
    message_t *msg_p;
    float expected_value = 0;
    char name_file[MAX_NAME_FILE];
    if ((semid = semget(SEM_ID, 1, 0)) < 0)
        sys_err("Ошибка!  Потомок не получил доступ к набору семафоров!\n");
    if ((shmid = shmget(SHM_ID, sizeof (message_t), 0)) < 0)
        sys_err("Ошибка! Потомок не получил доступ к разделяемой памяти!\n");
    if ((msg_p = (message_t *) shmat(shmid, 0, 0)) == NULL)
        sys_err("Ошибка! Потомок не подключил сегмент разделяемой памяти к адресному просранству!\n");
    strcpy(name_file, argv[1]);
    int fp, num_elem;
    if ((fp = fopen(name_file, "r")) == NULL) {
        printf("Ошибка при открытии файла!\n");
        exit(1);
    }
    fscanf(fp, "%d", &num_elem);
    printf("Количество элементов в массиве данных: %d\n", num_elem);
    float elems[num_elem];
    for (int i = 0; i < num_elem * 2; i = i + 2) {
        fscanf(fp, "%f", &elems[i]);
        fscanf(fp, "%f", &elems[i + 1]);
        expected_value += elems[i] * elems[i + 1];
    }
    printf("Математическое ожидание равно: %f\n", expected_value);
    while (semctl(semid, 0, GETVAL, 0) == 1 || msg_p->type == MSG_TYPE_NOT_EMPTY)
        printf("Потомок с именем файла %s находится в режиме ожидания\n", name_file);
    semctl(semid, 0, SETVAL, 1);
    msg_p->expected_value = expected_value;
    printf("Потомок записал в разделяемую память: %f\n", msg_p->expected_value);
    msg_p->type = MSG_TYPE_NOT_EMPTY;
    semctl(semid, 0, SETVAL, 0);
    shmdt(msg_p);
    exit(0);
}