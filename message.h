#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <stdio.h>
#include <string.h>

#define SEM_ID	2012  
#define SHM_ID	2013
#define PERMS	0666     
#define MAX_NAME_FILE	25
#define MSG_TYPE_EMPTY  0 
#define MSG_TYPE_NOT_EMPTY 1 
#define MSG_TYPE_FINISH 2
typedef struct
{
  int type;
  float expected_value;
} message_t;
//key_t key_sem_id, key_shm_id;