lab9_2: child parent
child.o: child.c
	gcc -Wall -g -c child.c -std=c99
parent.o: parent.c
	gcc -Wall -g -c parent.c -std=c99
child: child.o
	gcc -o child child.o
parent: parent.o
	gcc -o parent parent.o
clean:
	rm -rf parent.o parent child child.o 