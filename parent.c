#include <stdio.h>
#include <string.h>
#include "message.h"

void sys_err(char *msg)
{
    puts(msg);
    exit(1);
}

int main(int argc, char * argv[])
{
    int semid, shmid, num_files, j = 0;
    message_t *msg_p;
    printf("Введите количество файлов\n");
    scanf("%d", &num_files);
    pid_t PidsProcess[num_files];
    char name_files[num_files][2];
    for (int i = 0; i < num_files; i++) {
        sprintf(name_files[i], "%d", i + 1);
        printf("name_files=%s\n", name_files[i]);
    }
    if ((semid = semget(SEM_ID, 1, PERMS | IPC_CREAT)) < 0)
        sys_err("Ошибка! Набор семафоров не инициализирован\n");
    if ((shmid = shmget(SHM_ID, sizeof (message_t), PERMS | IPC_CREAT)) < 0)
        sys_err("Ошибка! Разделяемая память не инициализирована\n");
    if ((msg_p = (message_t *) shmat(shmid, 0, 0)) == NULL)
        sys_err("Ошибка! Разделяемая память не подключена к адресному пространству родителем!\n");
    semctl(semid, 0, SETVAL, 0);
    msg_p->type = MSG_TYPE_EMPTY;
    for (int i = 0; i < num_files; i++) {
        PidsProcess[i] = fork();
        if (-1 == PidsProcess[i]) {
            perror("Ошибка при создании процесса!\n");
            exit(1);
        } else if (0 == PidsProcess[i]) {
            if (execl("./child", "child", name_files[i], NULL) < 0) {
                printf("Ошибка при запуске процесса\n");
                exit(1);
            } else
                printf("Процесс №%d начал свою работу)\n", i + 1);
        }
    }

    while (1) {
        if (j == num_files)
            break;
        if (msg_p->type == MSG_TYPE_NOT_EMPTY) {
            if (semctl(semid, 0, GETVAL, 0) == 1)
            {
                continue;
                printf("Разделяемая память занята. Родитель в ожидании\n");
            }
            semctl(semid, 0, SETVAL, 1);
            j++;
            printf("Родитель получил из разделяемой памяти:\n");
            printf("%f\n", msg_p->expected_value);
            msg_p->type = MSG_TYPE_EMPTY;
            semctl(semid, 0, SETVAL, 0);       
        }
    }
    if (semctl(semid, 0, IPC_RMID, (struct semid_ds *) 0) < 0)
        sys_err("Ошибка при деинициализации семафоров!\n");
    shmdt(msg_p);
    if (shmctl(shmid, IPC_RMID, (struct shmid_ds *) 0) < 0)
        sys_err("Ошибка при деинициализации разделяемой памяти\n");
}